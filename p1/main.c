/* 
 * File:   pfork.c
 * Author: logan
 *
 * Created on 19 de octubre de 2020, 01:13 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  pid_t pid;
  pid = fork();
  switch(pid) {
    case -1:
      printf("ERROR.- No se pudo crear el proceso hijo\n");
      break;
    case 0:
      printf("Soy el P-Hijo, mi PID es %d y mi PPID es %d\n", getpid(), getppid());
      sleep(20);
      break;
    default:
      printf("Soy el P-Padre, mi PID es %d y el PID de mi hijo es %d\n", getpid(), pid);
      sleep(30);
  }
  printf("Final de ejecución de %d \n", getpid());
  return (EXIT_SUCCESS);
}